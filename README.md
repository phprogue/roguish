# README #

### What is this repository for? ###

* A tiny jQuery-ish library with minimal functionality for personal use and learning
* 0.1.0

### How do I get set up? ###

* Include the roguish.js to your page and let 'er rip
* No config, no dependencies, no database, nada

### Using this bad boy ###
* Roguish is mapped to 'R' like jQuery is to '$' because I hate extraneous typing
* Call a method to get the DOM element you want
* Chaining is allowed, because who doesn't love working on a chain gang?!
* That's it. Perhaps more to come in the near future.