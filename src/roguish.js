var Roguish = function() {
    var dom;
};
window.Roguish = Roguish;
window.R === undefined && (window.R = Roguish);

(function(R) {
    /*
     * Generic get function
     */
    R.get = function(selector) {
        if (selector[0] == '#') {
            this.dom = Array(document.getElementById(selector.slice(1)));
        } else if (selector[0] == '.') {
            this.dom = Array.from(document.getElementsByClassName(selector.slice(1)));
        } else {
            this.dom = Array.from(document.getElementsByTagName(selector));
        }
        return this;
    }
    R.getId = function(id) {
        this.dom = Array(document.getElementById(id));
        return this;
    };
    R.getClass = function(classname) {
        var elements = document.getElementsByClassName(classname);
        this.dom = Array.from(elements);
        return this;
    };
    R.getTag = function(tagname) {
        var elements = document.getElementsByTagName(tagname);
        this.dom = Array.from(elements);
        return this;
    };
    R.css = function(style, value) {
        this.dom.forEach(function(element, idx) {
            element.style[style] = value;
        });
        return this;
    };
    R.text = function(text) {
        this.dom.forEach(function(element, idx) {
            element.innerHTML = text;
        });
        return this;
    };
})(Roguish);
